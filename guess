#!/usr/bin/env bash

# Assign to variable "num" a value from 1 to 100.num=$(shuf -i 1-100 -n 1)
num=$(shuf -i 1-100 -n 1)
wrong=true
guesses=0

# function to declare the final number of guesses and include an appropriate insult.
# TODO: Read multiple insults for each category from a file, and choose one randomly.
final_output () {

  local totalGuesses=${1}

  echo

  if [[ ${totalGuesses} -eq 1 ]]
  then
    echo "You got it in only one guess!"
  else
    echo "You got it ... in ${totalGuesses} guesses."
  fi

  echo

  if [[ ${totalGuesses} -eq 1 ]]
  then
    echo "That was absolutely amazing ... for a human."
  elif [[ ${totalGuesses} -lt 6 ]]
  then
    echo "Not bad. Are you sure you are not a computer?"
  elif [[ ${totalGuesses} -lt 11 ]]
  then
    echo "Not that great. Aren't you humans supposed to be superior to us, or something?"
    echo "(Snickers in binary ...)"
  else
    # Historical note: This was an actual end-of-game insult in my encounter of this program
    # in about 1978, encountered in the Easton, Massachusetts, Oliver Ames High School's
    # computer lab, headed by the incomparable Donald "Doc" Harrison.
    echo "Pfah. My grandmother can do better than that."
  fi

  echo
}

clear

echo "I am thinking of a number between 1 and 100 (inclusive). Try to guess it."
echo "I will tell you if your guess is too high, too low, or just right."
echo

while ${wrong}
  do

    printf "Enter your guess: "
    read -r guess

    ((guesses=guesses+1))

    if [[ $guess -eq $num ]]
    then
      wrong=false

      # pass the number of guesses to the "final_ouput" function.
      final_output $guesses
    elif [[ $guess -lt $num ]]
    then
      echo "Too low."
    else
      echo "Too high."
    fi

  done

# One final comment: Always choose, "More Magic." http://www.catb.org/jargon/html/magic-story.html

